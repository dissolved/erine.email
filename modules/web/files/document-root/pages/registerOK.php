<?php
  if($user->isSigned()) redirect("/disposableMails");
?>
<?php
  $hash = $user->session->get("_data")["data"]["confirmation"];
  if (!empty($hash))
  {
    $email = $user->table->getRow(array('confirmation' => $hash))->mailAddress;
    if (!empty($email))
    {
      $subject = "Confirm your email address";
      $message  = "Welcome to erine.email,\n\n";
      $message .= "To complete the registration and gain full access to your account, please click this link or paste the address into your browser:\n";
      $message .= "https://erine.email/activateAccount?c=$hash\n\n";
      $message .= "See you there in a moment,\n\n";
      $message .= "Your erine.email robot";
      $header = "From:erine.email robot <robot@erine.email> \r\n";
      $retval = mail($email, $subject, $message, $header);
      if( $retval == true )
      {
?>
<div class="content-parent">
  <div class="content-child-middle">
    <div class="container marketing">
      <i class="fa-solid fa-paper-plane fa-ee-message"></i>
      <h2>Confirmation sent</h2>
      <p>A confirmation email had been sent to <?php echo $email; ?>. Please check your inbox and clic on the provided link to complete your registration.</p>
      <p><a class="btn btn-viewdetails" href="/" role="button">Home &raquo;</a></p>
    </div>
  </div>
</div>
<?php
      }
      else
      {
        redirect("/error");
      }
    }
    else
    {
      redirect("/error");
    }
  }
  else
  {
    redirect("/error");
  }
?>
