<!-- Carousel -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox" style="background-image: url('static/images/mailboxes.jpg'); background-size: cover; background-position-y: bottom;">
    <div class="item active" style="background-color: transparent;">
        <div class="carousel-caption">
          <h1>An anti-spam for your existing email address</h1>
          <p>erine.email is a strong shield for your emails against spam. Based on unlimited email addresses dedicated per usage, erine.email is completely free, open-source, and simple. Really.</p>
          <p><a class="btn btn-lg btn-primary" href="/register" role="button">Sign up today</a></p>
        </div>
    </div>
  </div>
</div>
<div class="container marketing">

  <!-- Three columns of text below the carousel -->
  <div class="row">
    <div class="col-lg-4">
      <i class="fa-solid fa-shield fa-ee-message"></i>
      <h2>Easy, therefore efficient</h2>
      <p>Do you know the KISS principle? It states that most systems work best if they are kept simple rather than made complicated. erine.email is simple. Have 1 minute? Let me show you how it works.</p>
      <p><a class="btn btn-viewdetails" href="#feature1" role="button">Check it out &raquo;</a></p>
    </div>
    <div class="col-lg-4">
      <i class="fa-solid fa-envelope fa-ee-message"></i>
      <h2>Stop checking your spam folder</h2>
      <p>- I didn't get your email.<br>- Have you checked your spam folder?<br>Sounds familiar? Stop with obsolete spam systems.</p>
      <p><a class="btn btn-viewdetails" href="#feature3" role="button">See how &raquo;</a></p>
    </div>
    <div class="col-lg-4">
      <i class="fa-brands fa-gitlab fa-ee-message"></i>
      <h2>Open source</h2>
      <p>A system that you can trust has nothing to hide. erine.email is open-source. Not only the code. The whole architecture too.</p>
      <p><a class="btn btn-viewdetails" href="https://gitlab.com/mdavranche/erine.email" target="_blank" role="button">See on GitLab &raquo;</a></p>
    </div>
  </div>

  <!-- Featurette -->
  <hr id="feature1" class="featurette-divider">
  <div class="row featurette">
    <div class="col-md-7">
      <h2 class="featurette-heading">A username and your email address.<span class="text-muted">That's all we need.</span></h2>
      <p class="lead">While registrating, I ask you to choose a username (let's say <span class="text-primary">joe</span>) and give your real email address (let's say <span class="text-primary">joe@nice.com</span>).</p>
      <p class="lead">The configuration part is over!</p>
    </div>
    <div class="col-md-5">
      <img class="featurette-image img-responsive center-block" src="/static/images/pic1.jpg" alt="1 address per contact">
    </div>
  </div>
  <p class="lead" style="margin-top: 60px;">Now when the <span class="text-primary">Brand42</span> shop asks for your email, answer <span class="text-primary">brand42.joe@erine.email</span>. All the emails sent to this address will be forwarded to yours. <span class="text-primary">Brand42</span> will have no idea of your real email address, even if you reply to him.</p>

  <!-- Featurette -->
  <hr id="feature2" class="featurette-divider">
  <div class="row featurette">
    <div class="col-md-7 col-md-push-5">
      <h2 class="featurette-heading">OK, what about spam?</h2>
      <p class="lead">So you gave your <span class="text-primary">brand42.joe@erine.email</span> mail address to <span class="text-primary">Brand42</span>, <span class="text-primary">jack.joe@erine.email</span> to your friend <span class="text-primary">Jack</span>, and so on.</p>
      <p class="lead">Now, when one of your email addresses had been compromised (you can not unsubscribe a newsletter, the <span class="text-primary">Badguys</span> shop sold your email address to spammers, somebody hacked your friend's mailbox...), just disable it in your web interface.</p>
    </div>
    <div class="col-md-5 col-md-pull-7">
      <img class="featurette-image img-responsive center-block" src="/static/images/pic2.jpg" alt="Disable the address you want">
    </div>
  </div>
  <p class="lead" style="margin-top: 60px;">Yes. That means you will know who gave your email to spammers!</p>

  <!-- Featurette -->
  <hr id="feature3" class="featurette-divider">
  <div class="row featurette">
    <div class="col-md-7">
      <h2 class="featurette-heading">No more false positive <span class="text-muted">or false negative</span>.</h2>
      <p class="lead">Now that you're using <span class="text-primary">erine.email</span>, you simply disable compromised email addresses. That means you won't have spam anymore.<p>
      <p class="lead">As this system is efficient, you won't need your mail provider's spam predictor and can disable it with this simple rule: everything sent to <span class="text-primary">something@erine.email</span> is not a spam.</p>
    </div>
    <div class="col-md-5">
      <img class="featurette-image img-responsive center-block" src="/static/images/pic3.jpg" alt="No false positive">
    </div>
  </div>
  <p class="lead" style="margin-top: 60px;">Your spam folder will be empty, forever.</p>

  <hr class="featurette-divider">
</div>
