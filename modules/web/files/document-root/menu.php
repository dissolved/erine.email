<?php

// Menu content
$menu = [
          [ "About / contact", "about", False ],
          [ "Documentation", "https://gitlab.com/mdavranche/erine.email/wikis/home", False ],
          [ "Donate", "https://www.paypal.com/donate/?hosted_button_id=5XRH448JXTF8S", False ]
        ];
if ($user->isSigned())
{
  array_push($menu, [ "My account", [
                                      [ "My profile", "comingsoon", "fa-user"],
                                      [ "My emails", "disposableMails", "fa-envelope"],
                                      False,
                                      [ "Log out", "actions/logout.php", "fa-sign-out"]
                                    ] ]);
}
else
{
  array_push($menu, [ "Log-in", "login", "fa-sign-in" ],
                    [ "Register", "register", "fa-star"]);
}

?>

<nav class="navbar navbar-default navbar-fixed-top navbar-ee topnav" role="navigation">
  <div class="container topnav">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand topnav" href="/">erine.email</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">

<?php
foreach ($menu as $item)
{
  if (is_array($item[1]))
  {
    print('<li>');
    print('<a class="nav-link dropdown-toggle" href="#" id="dropdownPremium" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . $item[0] . '</a>');
    print('<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownPremium">');
    foreach ($item[1] as $subitem)
    {
      if ($subitem)
      {
        print('<a class="dropdown-item');
        if ($pagePath == $subitem[1])
        {
          print(' active');
        }
        print('" href="' . $subitem[1] . '">');
        if ($subitem[2])
        {
          print('<i class="fa-solid ' . $subitem[2] . '"></i> ');
        }
        print($subitem[0] . '</a>');
      }
      else
      {
        print('<div class="dropdown-divider"></div>');
      }
    }
    print('</div>');
    print('</li>');
  }
  else
  {
    print('<li><a');
    if ($pagePath == $item[1])
    {
      print(' class="active"');
    }
    print(' href="' . $item[1] . '">');
    if ($item[2])
    {
      print('<i aria-hidden="true" class="fa ' . $item[2] . '"></i> ');
    }
    print($item[0] . '</a></li>');
  }
}
?>

      </ul>
    </div>
  </div>
</nav>
