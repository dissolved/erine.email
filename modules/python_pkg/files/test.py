#!/usr/bin/python3
"""erine_email

Copyright (C) 2017 Mikael Davranche

This file is part of erine.email project. https://erine.email

erine.email is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with erine.mail.  If not, see <http://www.gnu.org/licenses/>.
"""

import email.header
import email.utils
import email.message
import email.parser
import email.policy
import erine_email
import sqlalchemy
import unittest

from erine_email.common import BounceException


class TestAll(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        """Create the SQLite database and tables ; Set db_connection"""
        db_engine = sqlalchemy.create_engine("sqlite:///:memory:")
        self.db_connection = db_engine.connect().execution_options(autocommit=True)
        for query_str in [
            "CREATE TABLE `user` "
            "("
            "`ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
            "`username` varchar(15) NOT NULL UNIQUE, "
            "`reserved` tinyint(1) NOT NULL DEFAULT '0', "
            "`firstName` varchar(15) NOT NULL, "
            "`lastName` varchar(15) NOT NULL, "
            "`password` varchar(40) NOT NULL, "
            "`mailAddress` varchar(254) NOT NULL, "
            "`activated` tinyint(1) NOT NULL DEFAULT '0', "
            "`confirmation` char(40) NOT NULL DEFAULT '', "
            "`registrationDate` int(11) NOT NULL, "
            "`lastLogin` int(11) NOT NULL DEFAULT '0'"
            ")",
            "CREATE TABLE `disposableMailAddress` "
            "("
            "`mailAddress` varchar(254) NOT NULL PRIMARY KEY, "
            "`userID` int(7) NOT NULL, "
            "`created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, "
            "`enabled` tinyint(1) NOT NULL DEFAULT '1', "
            "`remaining` smallint(5) DEFAULT NULL, "
            "`sent` int(10) NOT NULL DEFAULT '0', "
            "`dropped` int(10) NOT NULL DEFAULT '0', "
            "`sentAs` int(10) NOT NULL DEFAULT '0', "
            "`comment` varchar(140) DEFAULT NULL, "
            "FOREIGN KEY (`userID`) REFERENCES `user` (`ID`)"
            ")",
            "CREATE TABLE `message` "
            "("
            "`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
            "`disposableMailAddress` varchar(254) DEFAULT NULL, "
            "`messageId` varchar(998) NOT NULL, "
            "`subject` varchar(998) NOT NULL, "
            "`from` varchar(2048) NOT NULL, "
            "`rcptTo` varchar(2048) NOT NULL, "
            "`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, "
            "`status` TEXT CHECK ( `status` IN ('sent', 'dropped', 'sentAs', 'looped')), "
            "FOREIGN KEY (`disposableMailAddress`) REFERENCES `disposableMailAddress` (`mailAddress`)"
            ")",
            "CREATE TABLE `replyAddress` "
            "("
            "`mailAddress` varchar(254) NOT NULL UNIQUE, "
            "`disposableMailAddress` varchar(254) NOT NULL, "
            "`foreignAddress` varchar(254) NOT NULL, "
            "UNIQUE (`disposableMailAddress`,`foreignAddress`), "
            "FOREIGN KEY (`disposableMailAddress`) REFERENCES `disposableMailAddress` (`mailAddress`)"
            ")",
            "PRAGMA foreign_keys = ON",
        ]:
            self.db_connection.execute(sqlalchemy.text(query_str))

    def _email_assertion(self, inbound, outbound):
        """Forge inbound email, generate and check the outbound one

        inbound:
            Information required to forge the inbound email
            Format: dictionary with 6 mandatory keys, string for values
        outbound:
            What is expected to be found on the outbound email
            Format: dictionary with 5 mandatory keys, [ string, is the string a regex? (boolean) ] for values
        """

        # Forge inbound email, generate the outbound one
        text_with_real_address = "abc def {0} ghi jkl".format(inbound["real_address"])
        message = email.parser.BytesParser(policy=email.policy.compat32).parsebytes(
            text_with_real_address.encode("utf-8")
        )
        message["Header-with-real-Address"] = text_with_real_address
        for header in ["From", "To", "Message-Id"]:
            message[header] = inbound[header]
        outbound_email = erine_email.email(
            message=message,
            sender=inbound["sender"],
            recipient=inbound["recipient"],
            db_connection=self.db_connection,
        )

        # Check outbound email sender
        if outbound["sender"][1]:
            self.assertRegex(outbound_email.outbound_sender, outbound["sender"][0])
        else:
            self.assertEqual(outbound_email.outbound_sender, outbound["sender"][0])

        # Check outbound email recipient ; Note that it is never a regex
        self.assertEqual(outbound_email.outbound_recipient, outbound["recipient"][0])

        # Check "From", "To", and "Message-Id" headers
        for header in ["From", "To", "Message-Id"]:
            if outbound[header][1]:
                self.assertRegex(outbound_email.outbound_message.get(header), outbound[header][0])
            else:
                self.assertEqual(outbound_email.outbound_message.get(header), outbound[header][0])

        # Check real address
        for item in outbound_email.outbound_message.items():
            self.assertFalse(inbound["real_address"] in item[1], "Real address found on {0} header".format(item[0]))
        outbound_email.outbound_message._headers = []
        body = outbound_email.outbound_message.as_string()
        self.assertTrue(inbound["real_address"] in body, "Real address should not be rewritten on body")

    def _exception_assertion(self, inbound_sender, inbound_recipient, message):
        """Check that the inbound_sender and outbound_recipient raise a BounceException with the given message"""
        with self.assertRaises(BounceException) as cm:
            erine_email.email(
                message=email.message.EmailMessage(),
                sender=inbound_sender,
                recipient=inbound_recipient,
                db_connection=self.db_connection,
            )
        self.assertEqual(str(cm.exception), message)

    def test_1_pythonbug_1(self):
        """Highlight a Python bug found after Python 3.7.3 (Debian 10) with email.policy.default

        Nothing is asserted here, as all we expect is that nothing is raised with the compat32 policy
        """
        faulty_header = "Message-ID: <[9f83415a58f84675bac8ab3e6047a157-JFBVALKQOJXWILKCJQZFA7CJIFGVGU2QKJ6FGU2QKJCW2YLJNR6EK6DPKNWXI4A=@example.com]>"
        message = email.parser.BytesParser(policy=email.policy.compat32).parsebytes(faulty_header.encode("utf-8"))
        message.as_bytes()

    def test_1_pythonbug_2(self):
        """Highlight a Python bug found in Python 3.7.3 (Debian 10), fixed in Python 3.9.3 (Debian 11)"""

        # 2 French words with accents RFC 2047 b and q encoded, UTF-8
        french_words_rfc_2047_b = "=?utf-8?b?TW96YcOvayBBY2PDqHM=?="
        french_words_rfc_2047_q = "=?utf-8?q?Moza=C3=AFk_Acc=C3=A8s?="

        # Check RFC 2047 encoding header
        message = email.message.EmailMessage()
        template = "A B {0} C abcdefghijklmnopqrstuvwxyz D {0} E F"
        message["subject"] = template.format(french_words_rfc_2047_q)
        message2 = email.parser.BytesParser(policy=email.policy.compat32).parsebytes(message.as_bytes())
        header_content = message2["Subject"].replace("\n", "")
        is_b_encoded = header_content == template.format(french_words_rfc_2047_b)
        is_q_encoded = header_content == template.format(french_words_rfc_2047_q)
        self.assertTrue(is_b_encoded or is_q_encoded)

    def test_1_unknown_format(self):
        "Unknown recipient format: {0}"
        self._exception_assertion(
            inbound_sender="X",
            inbound_recipient="Y",
            message="Unknown recipient format: Y",
        )

    def test_2_Classic_01_unknown_user(self):
        """Test Classic, unknown user name"""
        self._exception_assertion(
            inbound_sender="info@company.com",
            inbound_recipient="company.joe@erine.email",
            message="Classic - Unknown user name: joe",
        )

    def test_2_Classic_02_not_activated(self):
        """Create joe ; Test Classic, user not activated"""

        # Create joe
        query = sqlalchemy.text(
            "INSERT INTO `user` (`username`, `reserved`, `firstName`, `lastName`, `password`, `mailAddress`, `activated`, `registrationDate`) "
            "VALUES('joe', 0, 'Joe', 'Test', 'cf412240c9377079c4ef8f0be28df1f33a670e15', 'joe@example.com', 0, 'now')"
        )
        self.db_connection.execute(query)

        # Test exception
        self._exception_assertion(
            inbound_sender="info@company.com",
            inbound_recipient="company.joe@erine.email",
            message="Classic - The joe user had not been activated",
        )

    def test_2_Classic_03(self):
        """Activate joe ; Test Classic, normal case (twice), and empty Message-Id case"""

        # Activate joe
        query = sqlalchemy.text("UPDATE `user` SET confirmation = '', activated = 1 WHERE `username` = 'joe'")
        self.db_connection.execute(query)

        # Test normal case
        message_id = email.utils.make_msgid(domain="company.com")
        inbound = {
            "real_address": "joe@example.com",
            "sender": "info@company.com",
            "recipient": "company.joe@erine.email",
            "From": "Company marketing service <info@company.com>",
            "To": "Joe <company.joe@erine.email>",
            "Message-Id": message_id,
        }
        outbound = {
            "sender": ["^[a-z,0-9]{15}@erine\.email$", True],
            "recipient": ["joe@example.com", False],
            "From": ['^"Company marketing service - info@company.com" <[a-z,0-9]{15}@erine\.email>$', True],
            "To": ["Joe <company.joe@erine.email>", False],
            "Message-Id": [message_id, False],
        }
        self._email_assertion(inbound, outbound)

        # Test normal case again
        self._email_assertion(inbound, outbound)

        # Test empty Message-Id case
        inbound["Message-Id"] = ""
        outbound["Message-Id"] = [".+", True]
        self._email_assertion(inbound, outbound)

        # Test no label on From
        inbound["From"] = "info@company.com"
        outbound["From"] = ['^"info@company.com" <[a-z,0-9]{15}@erine\.email>$', True]
        self._email_assertion(inbound, outbound)

    def test_2_Classic_04_subject(self):
        """Test subject attribute (empty and non-empty cases)

        The test is done with Classic, but it could have been done with any other Generic child
        """
        message = email.message.EmailMessage()
        outbound_email = erine_email.email(
            message=message,
            sender="info@company.com",
            recipient="company.joe@erine.email",
            db_connection=self.db_connection,
        )
        self.assertEqual(outbound_email.subject, None)
        message["subject"] = "abcdef"
        outbound_email = erine_email.email(
            message=message,
            sender="info@company.com",
            recipient="company.joe@erine.email",
            db_connection=self.db_connection,
        )
        self.assertEqual(outbound_email.subject, message["subject"])

    def test_2_Classic_05_utf8(self):
        """Test dummy header and subject attribute, with Turkish letters, UTF-8 encoded

        Nothing is asserted about the dummy header, as all we expect is that nothing is raised there

        The test is done with Classic, but it could have been done with any other Generic child
        """

        # The "Turkish header value" (translated in Turkish) UTF-8 string
        turkish_header_value = b"T\xc3\xbcrk\xc3\xa7e ba\xc5\x9fl\xc4\xb1k de\xc4\x9feri".decode("utf-8")

        # Check UTF-8 string
        message = email.message.EmailMessage()
        message["dummy"] = turkish_header_value
        message["subject"] = turkish_header_value
        outbound_email = erine_email.email(
            message=message,
            sender="info@company.com",
            recipient="company.joe@erine.email",
            db_connection=self.db_connection,
        )
        self.assertEqual(outbound_email.subject, turkish_header_value)

        # The "Turkish header value" (translated in Turkish) string RFC 2047 encoded, UTF-8
        turkish_header_rfc_2047 = "=?utf-8?b?VMO8cmvDp2UgYmHFn2zEsWsgZGXEn2VyaQ==?="

        # Check RFC 2047 encoded string, UTF-8
        message = email.message.EmailMessage()
        message["dummy"] = turkish_header_rfc_2047
        message["subject"] = turkish_header_rfc_2047
        outbound_email = erine_email.email(
            message=message,
            sender="info@company.com",
            recipient="company.joe@erine.email",
            db_connection=self.db_connection,
        )
        self.assertEqual(outbound_email.subject, turkish_header_value)
        self.assertEqual(outbound_email.outbound_message["subject"], turkish_header_value)

        # The "Turkish header value" (translated in Turkish) string RFC 2047 encoded, UTF-16
        turkish_header_rfc_2047 = "=?utf-16?b?//5UAPwAcgBrAOcAZQAgAGIAYQBfAWwAMQFrACAAZABlAB8BZQByAGkA?="

        # Check RFC 2047 encoded string, UTF-16
        message = email.message.EmailMessage()
        message["dummy"] = turkish_header_rfc_2047
        message["subject"] = turkish_header_rfc_2047
        outbound_email = erine_email.email(
            message=message,
            sender="info@company.com",
            recipient="company.joe@erine.email",
            db_connection=self.db_connection,
        )
        self.assertEqual(outbound_email.subject, turkish_header_value)
        self.assertEqual(outbound_email.outbound_message["subject"], turkish_header_value)

    def test_3_Reserved_01_unknown_user(self):
        """Test Reserved, unknown user name"""
        self._exception_assertion(
            inbound_sender="info@company.com",
            inbound_recipient="bob@erine.email",
            message="Reserved - Unknown user name: bob",
        )

    def test_3_Reserved_02_not_activated(self):
        """Create bob ; Test Reserved, user not activated"""

        # Create bob
        query = sqlalchemy.text(
            "INSERT INTO `user`(`username`, `reserved`, `firstName`, `lastName`, `password`, `mailAddress`, `activated`, `registrationDate`) "
            "VALUES('bob', 1, 'Bob', 'Test', 'e9206d77416ea49bb0d5d7825ff1918cbea7675d', 'bob@example.com', 0, 'now')"
        )
        self.db_connection.execute(query)

        # Test exception
        self._exception_assertion(
            inbound_sender="info@company.com",
            inbound_recipient="bob@erine.email",
            message="Reserved - The bob user had not been activated",
        )

    def test_3_Reserved_03(self):
        """Activate bob ; Test Reserved, normal case (twice)"""

        # Activate bob
        query = sqlalchemy.text("UPDATE `user` SET confirmation = '', activated = 1 WHERE `username` = 'bob'")
        self.db_connection.execute(query)

        # Test normal case
        message_id = email.utils.make_msgid(domain="company.com")
        inbound = {
            "real_address": "bob@example.com",
            "sender": "info@company.com",
            "recipient": "bob@erine.email",
            "From": "Company marketing service <info@company.com>",
            "To": "Bob <bob@erine.email>",
            "Message-Id": message_id,
        }
        outbound = {
            "sender": ["^[a-z,0-9]{15}@erine\.email$", True],
            "recipient": ["bob@example.com", False],
            "From": ['^"Company marketing service - info@company.com" <[a-z,0-9]{15}@erine\.email>$', True],
            "To": ["Bob <bob@erine.email>", False],
            "Message-Id": [message_id, False],
        }
        self._email_assertion(inbound, outbound)

        # Test normal case again
        self._email_assertion(inbound, outbound)

    def test_4_Mix_Classic_Reserved(self):
        """Test mix between Classic and Reserved"""
        self._exception_assertion(
            inbound_sender="info@company.com",
            inbound_recipient="company.bob@erine.email",
            message="Classic - Incorrect user usage: bob exists, but as a reserved user",
        )
        self._exception_assertion(
            inbound_sender="info@company.com",
            inbound_recipient="joe@erine.email",
            message="Reserved - Incorrect user usage: joe exists, but not as a reserved user",
        )

    def _get_reply_joe(self):
        """Get a replyAddress associated to company.joe@erine.email

        Raise if not exists
        """
        query = sqlalchemy.text(
            "SELECT `mailAddress` FROM `replyAddress` WHERE `disposableMailAddress` = 'company.joe@erine.email'"
        )
        results = self.db_connection.execute(query)
        row = results.one_or_none()
        return row._mapping["mailAddress"]

    def test_5_Reply_01_not_allowed(self):
        """Test Reply, unauthorized email address"""
        self._exception_assertion(
            inbound_sender="unauthorized@example.com",
            inbound_recipient=self._get_reply_joe(),
            message="Reply - unauthorized@example.com is not allowed to send an email as company.joe@erine.email",
        )

    def test_5_Reply_02_not_activated(self):
        """Desactivate joe ; Test Reply, user not activated"""

        # Desactivate joe
        query = sqlalchemy.text("UPDATE `user` SET activated = 0 WHERE `username` = 'joe'")
        self.db_connection.execute(query)

        # Test exception
        self._exception_assertion(
            inbound_sender="joe@example.com",
            inbound_recipient=self._get_reply_joe(),
            message="Reply - The joe user had not been activated",
        )

    def test_5_Reply_03_Classic(self):
        """Reactivate joe ; Test Reply, normal case (twice), and forged From

        The labels are quoted only if they contain special characters like "@" and a ".". See the email.utils.formataddr
        source code for more information about this.
        """

        # Reactivate joe
        query = sqlalchemy.text("UPDATE `user` SET activated = 1 WHERE `username` = 'joe'")
        self.db_connection.execute(query)

        # Test normal case
        message_id = email.utils.make_msgid(domain="example.com")
        inbound = {
            "real_address": "joe@example.com",
            "sender": "joe@example.com",
            "recipient": self._get_reply_joe(),
            "From": "joe@example.com",
            "To": '"info@company.com" <{0}>,third_party@example.com'.format(self._get_reply_joe()),
            "Message-Id": message_id,
        }
        outbound = {
            "sender": ["company.joe@erine.email", False],
            "recipient": ["info@company.com", False],
            "From": ["company.joe@erine.email", False],
            "To": ["info@company.com,third_party@example.com", False],
            "Message-Id": [message_id, False],
        }
        self._email_assertion(inbound, outbound)

        # Test normal case again
        self._email_assertion(inbound, outbound)

        # Test other labels on To
        inbound["To"] = '"Company marketing service - info@company.com" <{0}>'.format(self._get_reply_joe())
        outbound["To"] = ["Company marketing service <info@company.com>", False]
        self._email_assertion(inbound, outbound)
        inbound["To"] = "Unexpected and probably forged label <{0}>".format(self._get_reply_joe())
        outbound["To"] = ["Unexpected and probably forged label <info@company.com>", False]
        self._email_assertion(inbound, outbound)

        # Test forged From
        inbound["From"] = "forged_from@example.com"
        outbound["From"] = ["forged_from@example.com", False]
        self._email_assertion(inbound, outbound)

    def test_5_Reply_04_Reserved(self):
        """Test Reply, normal case (twice)"""

        # Get a replyAddress associated to bob@erine.email
        query = sqlalchemy.text(
            "SELECT `mailAddress` FROM `replyAddress` WHERE `disposableMailAddress` = 'bob@erine.email'"
        )
        results = self.db_connection.execute(query)
        row = results.one_or_none()
        inbound_recipient = row._mapping["mailAddress"]

        # Test normal case
        message_id = email.utils.make_msgid(domain="example.com")
        inbound = {
            "real_address": "bob@example.com",
            "sender": "bob@example.com",
            "recipient": inbound_recipient,
            "From": "bob@example.com",
            "To": '"info@company.com" <{0}>,third_party@example.com'.format(inbound_recipient),
            "Message-Id": message_id,
        }
        outbound = {
            "sender": ["bob@erine.email", False],
            "recipient": ["info@company.com", False],
            "From": ["bob@erine.email", False],
            "To": ["info@company.com,third_party@example.com", False],
            "Message-Id": [message_id, False],
        }
        self._email_assertion(inbound, outbound)

        # Test normal case again
        self._email_assertion(inbound, outbound)

    def test_6_FirstShot_01_unknown_user(self):
        """Test FirstShot, unknown user name"""
        self._exception_assertion(
            inbound_sender="info@company.com",
            inbound_recipient="company.alice.info_company.com@erine.email",
            message="FirstShot - Unknown user name: alice",
        )

    def test_6_FirstShot_02_not_allowed(self):
        """Test FirstShot, unauthorized email address"""
        self._exception_assertion(
            inbound_sender="unauthorized@example.com",
            inbound_recipient="company.joe.info_company.com@erine.email",
            message="FirstShot - unauthorized@example.com is not allowed to send an email as company.joe@erine.email",
        )

    def test_6_FirstShot_03_reserved(self):
        """Test FirstShot mix between Classic and Reserved"""
        self._exception_assertion(
            inbound_sender="bob@example.com",
            inbound_recipient="company.bob.info_company.com@erine.email",
            message="FirstShot - Incorrect user usage: bob exists, but as a reserved user",
        )

    def test_6_FirstShot_04_not_activated(self):
        """Desactivate joe ; Test FirstShot, user not activated"""

        # Desactivate joe
        query = sqlalchemy.text("UPDATE `user` SET activated = 0 WHERE `username` = 'joe'")
        self.db_connection.execute(query)

        # Test exception
        self._exception_assertion(
            inbound_sender="joe@example.com",
            inbound_recipient="company.joe.info_company.com@erine.email",
            message="FirstShot - The joe user had not been activated",
        )

    def test_6_FirstShot_05(self):
        """Reactivate joe ; Test FirstShot, normal case (twice), and forged From"""

        # Reactivate joe
        query = sqlalchemy.text("UPDATE `user` SET activated = 1 WHERE `username` = 'joe'")
        self.db_connection.execute(query)

        # Test normal case
        message_id = email.utils.make_msgid(domain="example.com")
        inbound = {
            "real_address": "joe@example.com",
            "sender": "joe@example.com",
            "recipient": "company.joe.info_company.com@erine.email",
            "From": "joe@example.com",
            "To": "company.joe.info_company.com@erine.email,third_party@example.com",
            "Message-Id": message_id,
        }
        outbound = {
            "sender": ["company.joe@erine.email", False],
            "recipient": ["info@company.com", False],
            "From": ["company.joe@erine.email", False],
            "To": ["info@company.com,third_party@example.com", False],
            "Message-Id": [message_id, False],
        }
        self._email_assertion(inbound, outbound)

        # Test normal case again
        self._email_assertion(inbound, outbound)

        # Test forged From
        inbound["From"] = "forged_from@example.com"
        outbound["From"] = ["forged_from@example.com", False]
        self._email_assertion(inbound, outbound)

    def test_6_FirstShot_06_underscores(self):
        """Test FirstShot, Foreign address with more than 1 underscore (unusual)

        The one being interpreted as @ is the last one (_ are allowed in the local part, not in the domain one)
        """
        message_id = email.utils.make_msgid(domain="example.com")
        inbound = {
            "real_address": "joe@example.com",
            "sender": "joe@example.com",
            "recipient": "company.joe.a_b_c_company.com@erine.email",
            "From": "joe@example.com",
            "To": "company.joe.a_b_c_company.com@erine.email,third_party@example.com",
            "Message-Id": message_id,
        }
        outbound = {
            "sender": ["company.joe@erine.email", False],
            "recipient": ["a_b_c@company.com", False],
            "From": ["company.joe@erine.email", False],
            "To": ["a_b_c@company.com,third_party@example.com", False],
            "Message-Id": [message_id, False],
        }
        self._email_assertion(inbound, outbound)

    @classmethod
    def tearDownClass(self):
        """Close and delete the SQLite database"""
        self.db_connection.close()


if __name__ == "__main__":
    unittest.main()
