# Regular expressions used to match the recipient of different kinds of emails
classic_regex = "^([^@\.]+)\.([^@\.]+)@([^@]+)$"
reply_reserved_regex = "^([^@\.]+)@([^@]+)$"
firstshot_regex = "^([^@\.]+)\.([^@\.]+)\.([^@]+)_([^@]+)@([^@]+)$"


class BounceException(Exception):
    pass
