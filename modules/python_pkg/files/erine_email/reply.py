import re
import sqlalchemy

from erine_email.common import BounceException
from erine_email.generic import Generic
from functools import lru_cache


class Reply(Generic):
    """An erine.email user replies to an an email originally sent from a foreign address"""

    def _get_disposable_address(self, reply_address):
        """Retrieve and return disposableMailAddress matching reply_address, or None if not found"""
        query = sqlalchemy.text(
            "SELECT `disposableMailAddress` FROM `replyAddress` WHERE `mailAddress` = :mail_address"
        )
        results = self.db_connection.execute(query, mail_address=reply_address)
        row = results.one_or_none()
        return row._mapping["disposableMailAddress"] if row else None

    def _get_foreign_address(self, reply_address):
        """Retrieve and return foreignAddress matching reply_address, or None if not found"""
        query = sqlalchemy.text("SELECT `foreignAddress` FROM `replyAddress` WHERE `mailAddress` = :mail_address")
        results = self.db_connection.execute(query, mail_address=reply_address)
        row = results.one_or_none()
        return row._mapping["foreignAddress"] if row else None

    def _get_real_address(self, disposable_address):
        """Retrieve and return the real address of the user having disposable_address"""
        query = sqlalchemy.text(
            "SELECT `user`.`mailAddress` "
            "FROM `user` "
            "JOIN `disposableMailAddress` ON `user`.`ID` = `disposableMailAddress`.`userID` "
            "WHERE `disposableMailAddress`.`mailAddress` = :mail_address"
        )
        results = self.db_connection.execute(query, mail_address=disposable_address)
        row = results.one_or_none()
        return row._mapping["mailAddress"]

    @property
    @lru_cache(maxsize=None)
    def outbound_sender(self):

        # inbound_recipient is necessarily an existing reply addres: the email() function already checked that
        return self._get_disposable_address(self.inbound_recipient)

    @property
    @lru_cache(maxsize=None)
    def outbound_recipient(self):

        # inbound_recipient is necessarily an existing reply addres: the email() function already checked that
        return self._get_foreign_address(self.inbound_recipient)

    def check_usage_security(self):
        if self._get_real_address(self.outbound_sender) != self.inbound_sender:
            raise BounceException(
                "Reply - {0} is not allowed to send an email as {1}".format(self.inbound_sender, self.outbound_sender)
            )
        query = sqlalchemy.text(
            "SELECT `user`.`username`, `user`.`activated` "
            "FROM `user` "
            "JOIN `disposableMailAddress` ON `user`.`ID` = `disposableMailAddress`.`userID` "
            "WHERE `disposableMailAddress`.`mailAddress` = :mail_address"
        )
        results = self.db_connection.execute(query, mail_address=self.outbound_sender)
        row = results.one_or_none()
        if not row._mapping["activated"]:
            raise BounceException("Reply - The {0} user had not been activated".format(row._mapping["username"]))

    def rewrite_from_replyto(self, address):
        if self._get_real_address(self.outbound_sender) == address[1]:

            # inbound_recipient is necessarily an existing reply addres: the email() function already checked that
            return (address[0], self._get_disposable_address(self.inbound_recipient))

        return address

    def rewrite_to_cc_bcc(self, address):

        # Do not rewrite if not reply address or reply address but not matching the owner of outbound_sender (that means
        # multiple erine.email users are having a conversation)
        if self._get_disposable_address(address[1]) != self.outbound_sender:
            return address

        rewritten_address = self._get_foreign_address(address[1])
        r = re.match("(.+) - {0}$".format(rewritten_address), address[0])
        if r:
            rewritten_label = r.group(1)
        elif address[0] == rewritten_address:
            rewritten_label = ""
        else:
            rewritten_label = address[0]
        return (rewritten_label, rewritten_address)
