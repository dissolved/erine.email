# Install requirements to run the Python scripts
class python_requirements {

  # pip, required to install Python packages in a more recent version than the one proposed by Debian
  package { 'python3-pip':
    ensure => present,
  }

  # Dropbox Python SDK, required by daily-bkp.py
  file { '/usr/local/etc/pip3_dropbox_requirements.txt':
    ensure => present,
    group  => root,
    owner  => root,
    mode   => '0644',
    source => 'puppet:///modules/python_requirements/pip3_dropbox_requirements.txt',
  }
  exec { 'pip3_dropbox':
    user        => 'root',
    group       => 'root',
    command     => '/usr/bin/pip3 install --requirement /usr/local/etc/pip3_dropbox_requirements.txt',
    subscribe   => File['/usr/local/etc/pip3_dropbox_requirements.txt'],
    refreshonly => true,
    require     => [
      File['/usr/local/etc/pip3_dropbox_requirements.txt'],
      Package['python3-pip'],
    ],
  }

  # SQLAlchemy, required by the erine_email Python package and its unit tests, and by spameater.py
  file { '/usr/local/etc/pip3_SQLAlchemy_requirements.txt':
    ensure => present,
    group  => root,
    owner  => root,
    mode   => '0644',
    source => 'puppet:///modules/python_requirements/pip3_SQLAlchemy_requirements.txt',
  }
  exec { 'pip3_SQLAlchemy':
    user        => 'root',
    group       => 'root',
    command     => '/usr/bin/pip3 install --requirement /usr/local/etc/pip3_SQLAlchemy_requirements.txt',
    subscribe   => File['/usr/local/etc/pip3_SQLAlchemy_requirements.txt'],
    refreshonly => true,
    require     => [
      File['/usr/local/etc/pip3_SQLAlchemy_requirements.txt'],
      Package['python3-pip'],
    ],
  }

  # MySQLdb, required by SQLAlchemy to connect to the MariaDB server
  package { 'python3-mysqldb':
    ensure => present,
  }

}
