# erine.email

*erine.email* is a strong shield for your emails against spam. Based on unlimited disposable email addresses, *erine.email* is completely free, open-source, and simple. Really.

The solution is available in [SaaS](https://en.wikipedia.org/wiki/Software_as_a_service) and [on-premises](https://en.wikipedia.org/wiki/On-premises_software).

SaaS: [Check this out!](https://erine.email)

On-premises: This repository is the piece of software that you need to host *erine.email* on your own server.

## Example

Let's say you own the email address **joe@example.org**, and you created the user **joe** on your *erine.email* instance.

You're about to buy a new vacuum cleaner from **brand42** online shop and you have to give them your email address. Communicate them your *erine.email* address instead!

Indeed, emails sent to **brand42.joe@erine.email** will be redirected automatically to **joe@example.org**. Your personal address is kept private, and you can disable **brand42.joe** in the future in case **brand42** suddenly decides to spam you.

## Documentation

The whole documentation is on [the wiki](https://gitlab.com/mdavranche/erine.email/-/wikis/home)!
